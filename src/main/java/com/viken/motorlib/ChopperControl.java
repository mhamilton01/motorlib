package com.viken.motorlib;

import android.util.Log;

public class ChopperControl {

    private MaxonDrv md;
    private boolean connected;
    private int node = 1;
    private String handle;

    public ChopperControl(String host) {
        md = new MaxonDrv(host);
        connected = false;
    }

    public boolean connect() {
        boolean status = true;
        if (!connected) {
            status = OpenDevice();
            if (status)
                status = SetupEC60();
            if (status)
                connected = true;
        }
        return status;
    }

    public boolean disconnect() {
        disable();
        connected = false;
        return true;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean enable(int rpm) {
        Log.d("ChopperControl enable", "requested rpm " + rpm);
        boolean status = true;
        if (status) {
            MaxonDrv.ACK ack;
            MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetEnableState.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
            cmd.name = MaxonTypes.Command.VCS_ActivateProfileVelocityMode.string();
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
            cmd.name = MaxonTypes.Command.VCS_MoveWithVelocity.string();
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            cmd.arg3 = String.valueOf(rpm);
            ack = md.Command(cmd);
            if (ack == null || ack.status == null)
                return status;
            if (ack.status.equals("")) {
            }
        }
        return status;
    }

    public boolean disable() {
        boolean status = false;
        if (connected) {
            MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetDisableState.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            md.Command(cmd);
            status = true;
        }
        return status;
    }

    private boolean isEnabled() {
        boolean status = false;
        if (connected) {
            MaxonDrv.ACK ack;
            MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetDisableState.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
            if (ack == null || ack.response1 == null)
                return false;
            if (ack.response1.equals("1")) {
                status = true;
            }
        }
        return status;
    }

    public int getRpm() {
        if (!connected)
            return 0;
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd;
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetVelocityIsAveraged.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null)
            return 0;
        int rpmAvg = md.tryParse(ack.response1);
        return rpmAvg;
    }

    public int getCurrent() {
        if (!connected)
            return 0;
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd;
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetCurrentIsAveraged.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null)
            return 0;
        int currAvg = md.tryParse(ack.response1);
        return currAvg;
    }

    public void Test() {
        enable(50);
        disable();

        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string() + " " + ack.response1);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string() + " " + ack.response1);

        /*
        2019-08-08 14:03:15.524 13197-13240/com.viken.smc D/Test: EG_PIVC_P_GAIN 269114
        2019-08-08 14:03:15.537 13197-13240/com.viken.smc D/Test: EG_PIVC_I_GAIN 400
        2019-08-08 14:03:15.552 13197-13240/com.viken.smc D/Test: EG_PIVC_FEED_FORWARD_VELOCITY_GAIN 52165
        2019-08-08 14:03:15.571 13197-13240/com.viken.smc D/Test: EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN 300
        */
    }

    private boolean OpenDevice() {
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_OpenDevice.string());
        cmd.arg1 = "EPOS4";
        cmd.arg2 = "MAXON SERIAL V2";
        cmd.arg3 = "USB";
        cmd.arg4 = "USB0";
        cmd.arg5 = "";
        ack = md.Command(cmd);
        if (ack == null || ack.status.isEmpty() || ack.status == "0") { return false; } // failed to open device
        handle = ack.status;
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetProtocolStackSettings.string());
        cmd.arg1 = handle;
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null || ack.response1.isEmpty())
            return false;
        if ((Integer.valueOf(ack.response1) == 1000000) && (Integer.valueOf(ack.response2) == 500)) {
            // success
        } else {
            cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetProtocolStackSettings.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(1000000); // set baudrate
            cmd.arg3 = "500";
            ack = md.Command(cmd);
            Log.e("OpenDevice", "reset baudrate = 1000000 and timeout = 500");
        }
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetFaultState.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null)
            return false;
        if (ack.response1.equals("1")) {
            Log.d("OpenDevice", "node " + cmd.arg1 + " fault");
            cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_ClearFault.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
        }
        connected = true;
        return true;
    }

    private boolean SetupEC60() {
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_ResetDevice.string());
        //cmd.arg1 = handle;
        //cmd.arg2 = String.valueOf(node);
        //ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_GetState.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        if (!ack.response1.equals(MaxonTypes.State.ST_ENABLED.string())) {
            disable();
        }
        // motor type
        //        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetMotorType.string());
        //        cmd.arg1 = handle;
        //        cmd.arg2 = String.valueOf(node);
        //        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMotorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.MotorType.MT_EC_BLOCK_COMMUTATED_MOTOR.string();
        ack = md.Command(cmd);

        // ec motor params
        //cmd.name = MaxonTypes.Command.VCS_GetEcMotorParameter.string();
        //cmd.arg1 = handle;
        //cmd.arg2 = String.valueOf(node);
        //ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetEcMotorParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(4300); // mA nominal continuous
        //cmd.arg4 = String.valueOf(2200); // mA max current
        cmd.arg4 = String.valueOf(78000); // mA max current
        cmd.arg5 = String.valueOf(400); // time constant, 1/10 seconds 400 = 40.0 seconds
        cmd.arg6 = String.valueOf(7); // pole pairs
        ack = md.Command(cmd);

        // sensor type
        cmd.name = MaxonTypes.Command.VCS_GetSensorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetSensorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.SensorType.ST_HALL_SENSORS.string();
        ack = md.Command(cmd);

/*
        cmd.name = MaxonTypes.Command.VCS_GetControllerGain.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string();
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetControllerGain.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string();
        ack = md.Command(cmd);

*/
        // max acceleration 50 rpm/s
        cmd.name = MaxonTypes.Command.VCS_GetMaxAcceleration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMaxAcceleration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(50);
        ack = md.Command(cmd);

        // max velocity 6000 rpm
        cmd.name = MaxonTypes.Command.VCS_GetMaxProfileVelocity.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMaxProfileVelocity.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(6000);
        ack = md.Command(cmd);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string();
        cmd.arg5 = String.valueOf(1458426);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string();
        cmd.arg5 = String.valueOf(690938);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string();
        cmd.arg5 = String.valueOf(28104);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string();
        cmd.arg5 = String.valueOf(784510);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string();
        cmd.arg5 = String.valueOf(1170);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string();
        cmd.arg5 = String.valueOf(3900);
        ack = md.Command(cmd);
        if (ack == null || ack.status == null)
            return false;
        if (ack.status.equals("")) {
        }

        return true;
    }

}
