package com.viken.motorlib;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class MaxonDrv {

    private String TAG = "MaxonDrv";
    private String host = "192.168.1.44";
    private int port = 7735;

    public MaxonDrv(String host) {
        this.host = host;
    }

    public void IpAddress(String ip) {
        host = ip;
    }

    public ACK Command(CMD cmd) {
        Gson gson = new Gson();
        String json = gson.toJson(cmd);
        //Log.d("MaxonDrv", "json " + json);
        ACK ack = send(json);
        //Log.d("Command", "cmd " + cmd.name + " ack.status " + ack.status + " ack.error 0x" + Integer.toHexString(Integer.valueOf(ack.error)) + " ack.response1 " + ack.response1);
        return ack;
    }

    private ACK send(String json) {
        int SOCKET_TIMEOUT = 5000;
        ACK ack = null;
        int size = json.length();
        final Socket socket = new Socket();
        try {
            socket.bind(null);
            socket.connect(new InetSocketAddress(host, port), SOCKET_TIMEOUT);
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            int s = size & 0xff;
            dos.writeByte(s); // low byte
            s = (size >> 8) & 0xff;
            dos.writeByte(s); // high byte
            dos.write(json.getBytes());
            dos.flush();
            ack = receive(socket);
        } catch (IOException e) {
            Log.e("send", "IOException " + e.getMessage() + " - " + host + ":" + port);
        }
        return ack;
    }

    private ACK receive(Socket socket) {
        ACK ack = new ACK();
        try {
            int cnt;
            byte[] b = new byte[1024];
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            cnt = dis.read(b, 0, 4);
            int rdCnt = ByteBuffer.wrap(b).order(ByteOrder.LITTLE_ENDIAN).getInt();
            cnt = dis.read(b, 0, rdCnt);
            if (cnt != rdCnt) {
                Log.e(TAG, "network read expected " + rdCnt + ", only read " + cnt);
            } else {
                JSONObject obj = new JSONObject(new String(b, "UTF-8"));
                ack.status = obj.getString("status");
                ack.error = obj.getString("error");
                ack.response1 = obj.getString("response1");
                ack.response2 = obj.getString("response2");
                ack.response3 = obj.getString("response3");
                ack.response4 = obj.getString("response4");
                ack.response5 = obj.getString("response5");
                ack.response6 = obj.getString("response6");
                ack.response7 = obj.getString("response7");
                ack.response8 = obj.getString("response8");
                ack.response9 = obj.getString("response9");
                ack.response10 = obj.getString("response10");
            }
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "IOException " + e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ack;
    }

    public static Integer tryParse(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public class CMD {
        String name;
        String arg1;
        String arg2;
        String arg3;
        String arg4;
        String arg5;
        String arg6;
        String arg7;
        String arg8;
        String arg9;
        String arg10;

        public CMD(String name) {
            this.name = name;
        }
    }

    public class ACK {
        String status;
        String error;
        String response1;
        String response2;
        String response3;
        String response4;
        String response5;
        String response6;
        String response7;
        String response8;
        String response9;
        String response10;

        public ACK() {
            status = "-2";
            error = "-2";
            response1 = "-2";
            response2 = "-2";
            response3 = "-2";
            response4 = "-2";
            response5 = "-2";
            response6 = "-2";
            response7 = "-2";
            response8 = "-2";
            response9 = "-2";
            response10 = "-2";
        }
    }

    public CMD InstantiateCMD(String name) {
        CMD cmd = new CMD(name);
        cmd.arg1 = "";
        cmd.arg2 = "";
        cmd.arg3 = "";
        cmd.arg4 = "";
        cmd.arg5 = "";
        cmd.arg6 = "";
        cmd.arg7 = "";
        cmd.arg8 = "";
        cmd.arg9 = "";
        cmd.arg10 = "";
        return cmd;
    }

}