package com.viken.motorlib;

import android.util.Log;

public class ShutterControl {

    private MaxonDrv md;
    private boolean connected;
    private int node = 2;
    private String handle;

    public ShutterControl(String host) {
        md = new MaxonDrv(host);
        connected = false;
    }

    public boolean connect() {
        boolean status = false;
        if (!connected) {
            status = OpenDevice();
            if (status) {
                status = SetupDC16();
            }
            if (status) {
                connected = true;
            }
        }
        return status;
    }

    public boolean disconnect() {
        connected = false;
        return true;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean Open() {
        boolean status = true;
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetEnableState.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetOperationMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.OperationMode.OMD_HOMING_MODE.string();
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_ActivateHomingMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_GetHomingParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetHomingParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(100000); // homing accel
        cmd.arg4 = String.valueOf(5000); // speed during search for switch
        cmd.arg5 = String.valueOf(500);
        cmd.arg6 = String.valueOf(150000); // home offset
        cmd.arg7 = String.valueOf(300); // current threshold
        cmd.arg8 = String.valueOf(0);
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_FindHome.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.HomingMethod.HM_NEGATIVE_LIMIT_SWITCH.string();
        ack = md.Command(cmd);

        return status;
    }

    public boolean Close() {
        boolean status = true;
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetEnableState.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetOperationMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.OperationMode.OMD_HOMING_MODE.string();
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_ActivateHomingMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_GetHomingParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetHomingParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(100000); // homing accel
        cmd.arg4 = String.valueOf(5000); // speed during search for switch
        cmd.arg5 = String.valueOf(500);
        cmd.arg6 = String.valueOf(150000); // home offset
        cmd.arg7 = String.valueOf(300); // current threshold
        cmd.arg8 = String.valueOf(0);
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_FindHome.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.HomingMethod.HM_POSITIVE_LIMIT_SWITCH.string();
        ack = md.Command(cmd);

        return status;
    }

    public boolean PercentOpen(int percent) {
        boolean status = false;
        if ((percent < 1) || (percent > 99)) {
            return status;
        }
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetEnableState.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetOperationMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.OperationMode.OMD_POSITION_MODE.string();
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_ActivateProfilePositionMode.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);

        cmd.name = MaxonTypes.Command.VCS_SetPositionProfile.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(10000);
        cmd.arg4 = String.valueOf(100000); // accel
        cmd.arg5 = String.valueOf(100000); // decel
        ack = md.Command(cmd);

        int move = (int) (-1f * 6900000f * (float) percent / 100f); // max incremements = 6900000, -1 is direction relative to close position
        Log.d("Shutter PercentOpen", "percent " + percent + " incs " + move);
        cmd.name = MaxonTypes.Command.VCS_MoveToPosition.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(move);
        cmd.arg4 = String.valueOf("True"); // absolute
        cmd.arg5 = String.valueOf("True"); // start immediately
        ack = md.Command(cmd);

        return status;
    }

    public void WaitWhileHoming() {
        boolean homingInProcess = true;

        while (homingInProcess) {
            MaxonDrv.ACK ack;
            MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetHomingState.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
            if (ack == null || ack.response1 == null)
                return;
            if (ack.response1.equals(MaxonTypes.HomingStatus.True.string())) {
                homingInProcess = false;
            } else if (ack.response2.equals(MaxonTypes.HomingStatus.True.string())) {
                Log.e("Shutter.Open", "homing error " + ack.response3);
                homingInProcess = false;
            }
            try { Thread.sleep(250); } catch (Exception e) { }
        }
        return;
    }

    public void WaitWhileMoving(int percent) {
        boolean moveInProcess = true;

        int move = (int) (-1f * 6900000f * (float) percent / 100f); // max incremements = 6900000, -1 is direction relative to close position
        while (moveInProcess) {
            MaxonDrv.ACK ack;
            MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetTargetPosition.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
            if (ack == null || ack.response1 == null)
                return;
            int actual = Integer.valueOf(ack.response1);
            Log.d("Shutter waitWhileMoving", "move " + move + " actual " + actual);
            if (actual == move) {
                moveInProcess = false;
            }
            try { Thread.sleep(250); } catch (Exception e) { }
        }
        return;
    }

    public void Test() {
        OpenDevice();
        SetupDC16();

        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string() + " " + ack.response1);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string() + " " + ack.response1);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string();
        ack = md.Command(cmd);
        Log.d("Test", MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string() + " " + ack.response1);

        /*
        2019-08-08 14:03:15.524 13197-13240/com.viken.smc D/Test: EG_PIVC_P_GAIN 269114
        2019-08-08 14:03:15.537 13197-13240/com.viken.smc D/Test: EG_PIVC_I_GAIN 400
        2019-08-08 14:03:15.552 13197-13240/com.viken.smc D/Test: EG_PIVC_FEED_FORWARD_VELOCITY_GAIN 52165
        2019-08-08 14:03:15.571 13197-13240/com.viken.smc D/Test: EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN 300
        */
    }

    private boolean OpenDevice() {
        MaxonDrv.ACK ack;
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_OpenDevice.string());
        cmd.arg1 = "EPOS4";
        cmd.arg2 = "MAXON SERIAL V2";
        cmd.arg3 = "USB";
        cmd.arg4 = "USB0";
        cmd.arg5 = "";
        ack = md.Command(cmd);
        if (ack == null || ack.status == null)
            return false;
        if (ack.status == null) { return false; }
        if (ack.status == "0") { return false; } // failed to open device
        handle = ack.status;
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetProtocolStackSettings.string());
        cmd.arg1 = handle;
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null || ack.response1.isEmpty())
            return false;
        if ((Integer.valueOf(ack.response1) == 1000000) && (Integer.valueOf(ack.response2) == 500)) {
            // success
        } else {
            cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetProtocolStackSettings.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(1000000); // set baudrate
            cmd.arg3 = "500";
            ack = md.Command(cmd);
            Log.e("OpenDevice", "reset baudrate = 1000000 and timeout = 500");
        }
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetFaultState.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null)
            return false;
        if (ack.response1.equals("1")) {
            Log.d("OpenDevice", "node " + cmd.arg1 + " fault");
            cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_ClearFault.string());
            cmd.arg1 = handle;
            cmd.arg2 = String.valueOf(node);
            ack = md.Command(cmd);
        }

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_OpenSubDevice.string());
        cmd.arg1 = handle;
        cmd.arg2 = "EPOS4";
        cmd.arg3 = "CANopen";
        ack = md.Command(cmd);
        if (ack == null || ack.response1 == null)
            return false;
        if (ack.status == "0") { return false; } // failed to open device
        handle = ack.status;
        connected = true;
        return true;
    }

    private boolean SetupDC16() {
        MaxonDrv.ACK ack;

        // motor type
        MaxonDrv.CMD cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_GetMotorType.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMotorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.MotorType.MT_DC_MOTOR.string();
        ack = md.Command(cmd);

        // ec motor params
        cmd.name = MaxonTypes.Command.VCS_GetDcMotorParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetDcMotorParameter.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(320); // mA
        cmd.arg4 = String.valueOf(1200); // mA
        cmd.arg5 = String.valueOf(2270); // time constant motor, 1/10 seconds 227 = 227.0 seconds
        ack = md.Command(cmd);

        // gear 83:1
        // max continuous input speed 10000rpm

        // sensor type
        cmd.name = MaxonTypes.Command.VCS_GetSensorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetSensorType.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        // digital incremental encoder 1 on motor shaft, maxon encoder with index (3 chan)
        cmd.arg3 = MaxonTypes.SensorType.ST_INC_ENCODER_3CHANNEL.string();
        ack = md.Command(cmd);

        // max acceleration 1000000 rpm/s
        cmd.name = MaxonTypes.Command.VCS_GetMaxAcceleration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMaxAcceleration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(1000000);
        ack = md.Command(cmd);

        // max velocity 5000 rpm
        cmd.name = MaxonTypes.Command.VCS_GetMaxProfileVelocity.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        ack = md.Command(cmd);
        cmd.name = MaxonTypes.Command.VCS_SetMaxProfileVelocity.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(5000);
        ack = md.Command(cmd);

        // following error 2000 inc

        // digital input 1, negative limit switch (active low)
        cmd.name = MaxonTypes.Command.VCS_DigitalInputConfiguration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(1); // input number
        cmd.arg4 = MaxonTypes.DigitalInputConfig.DIC_NEGATIVE_LIMIT_SWITCH.string();
        cmd.arg5 = MaxonTypes.Mask.True.string();
        cmd.arg6 = MaxonTypes.Polarity.ActiveLow.string();
        cmd.arg7 = MaxonTypes.Mask.False.string();
        ack = md.Command(cmd);
        // digital input 2, positive limit switch (active low)
        cmd.name = MaxonTypes.Command.VCS_DigitalInputConfiguration.string();
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = String.valueOf(2); // input number
        cmd.arg4 = MaxonTypes.DigitalInputConfig.DIC_POSITIVE_LIMIT_SWITCH.string();
        cmd.arg5 = MaxonTypes.Mask.True.string();
        cmd.arg6 = MaxonTypes.Polarity.ActiveLow.string();
        cmd.arg7 = MaxonTypes.Mask.False.string();
        ack = md.Command(cmd);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_P_GAIN.string();
        cmd.arg5 = String.valueOf(10306560);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_CURRENT_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PICC_I_GAIN.string();
        cmd.arg5 = String.valueOf(247897767);
        ack = md.Command(cmd);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_P_GAIN.string();
        cmd.arg5 = String.valueOf(2695);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_I_GAIN.string();
        cmd.arg5 = String.valueOf(391600);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_VELOCITY_GAIN.string();
        cmd.arg5 = String.valueOf(0);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PI_VELOCITY_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN.string();
        cmd.arg5 = String.valueOf(5);
        ack = md.Command(cmd);

        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PID_POSITION_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIDPC_P_GAIN.string();
        cmd.arg5 = String.valueOf(308392);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PID_POSITION_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIDPC_I_GAIN.string();
        cmd.arg5 = String.valueOf(3737812);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PID_POSITION_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIDPC_D_GAIN.string();
        cmd.arg5 = String.valueOf(2120);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PID_POSITION_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIDPC_FEED_FORWARD_VELOCITY_GAIN.string();
        cmd.arg5 = String.valueOf(0);
        ack = md.Command(cmd);
        cmd = md.InstantiateCMD(MaxonTypes.Command.VCS_SetControllerGain.string());
        cmd.arg1 = handle;
        cmd.arg2 = String.valueOf(node);
        cmd.arg3 = MaxonTypes.ControllerGain.EC_PID_POSITION_CONTROLLER.string();
        cmd.arg4 = MaxonTypes.ControllerGain.EG_PIDPC_FEED_FORWARD_ACCELERATION_GAIN.string();
        cmd.arg5 = String.valueOf(5);
        ack = md.Command(cmd);

        return true;
    }
}
