package com.viken.motorlib;

public class MaxonTypes {

    public enum Command {
        VCS_ActivateAnalogCurrentSetpoint("VCS_ActivateAnalogCurrentSetpoint"),
        VCS_ActivateAnalogPositionSetpoint("VCS_ActivateAnalogPositionSetpoint"),
        VCS_ActivateAnalogVelocitySetpoint("VCS_ActivateAnalogVelocitySetpoint"),
        VCS_ActivateCurrentMode("VCS_ActivateCurrentMode"),
        VCS_ActivateHomingMode("VCS_ActivateHomingMode"),
        VCS_ActivateInterpolatedPositionMode("VCS_ActivateInterpolatedPositionMode"),
        VCS_ActivateMasterEncoderMode("VCS_ActivateMasterEncoderMode"),
        VCS_ActivatePositionCompare("VCS_ActivatePositionCompare"),
        VCS_ActivatePositionMarker("VCS_ActivatePositionMarker"),
        VCS_ActivatePositionMode("VCS_ActivatePositionMode"),
        VCS_ActivateProfilePositionMode("VCS_ActivateProfilePositionMode"),
        VCS_ActivateProfileVelocityMode("VCS_ActivateProfileVelocityMode"),
        VCS_ActivateStepDirectionMode("VCS_ActivateStepDirectionMode"),
        VCS_ActivateVelocityMode("VCS_ActivateVelocityMode"),
        VCS_AddPvtValueToIpmBuffer("VCS_AddPvtValueToIpmBuffer"),
        VCS_AnalogInputConfiguration("VCS_AnalogInputConfiguration"),
        VCS_AnalogOutputConfiguration("VCS_AnalogOutputConfiguration"),
        VCS_ClearFault("VCS_ClearFault"),
        VCS_ClearIpmBuffer("VCS_ClearIpmBuffer"),
        VCS_CloseAllDevices("VCS_CloseAllDevices"),
        VCS_CloseAllSubDevices("VCS_CloseAllSubDevices"),
        VCS_CloseDevice("VCS_CloseDevice"),
        VCS_CloseSubDevice("VCS_CloseSubDevice"),
        VCS_DeactivateAnalogCurrentSetpoint("VCS_DeactivateAnalogCurrentSetpoint"),
        VCS_DeactivateAnalogPositionSetpoint("VCS_DeactivateAnalogPositionSetpoint"),
        VCS_DeactivateAnalogVelocitySetpoint("VCS_DeactivateAnalogVelocitySetpoint"),
        VCS_DeactivatePositionCompare("VCS_DeactivatePositionCompare"),
        VCS_DeactivatePositionMarker("VCS_DeactivatePositionMarker"),
        VCS_DefinePosition("VCS_DefinePosition"),
        VCS_DigitalInputConfiguration("VCS_DigitalInputConfiguration"),
        VCS_DigitalOutputConfiguration("VCS_DigitalOutputConfiguration"),
        VCS_DisableAnalogCurrentSetpoint("VCS_DisableAnalogCurrentSetpoint"),
        VCS_DisableAnalogPositionSetpoint("VCS_DisableAnalogPositionSetpoint"),
        VCS_DisableAnalogVelocitySetpoint("VCS_DisableAnalogVelocitySetpoint"),
        VCS_DisablePositionCompare("VCS_DisablePositionCompare"),
        VCS_DisablePositionWindow("VCS_DisablePositionWindow"),
        VCS_DisableVelocityWindow("VCS_DisableVelocityWindow"),
        VCS_EnableAnalogCurrentSetpoint("VCS_EnableAnalogCurrentSetpoint"),
        VCS_EnableAnalogPositionSetpoint("VCS_EnableAnalogPositionSetpoint"),
        VCS_EnableAnalogVelocitySetpoint("VCS_EnableAnalogVelocitySetpoint"),
        VCS_EnablePositionCompare("VCS_EnablePositionCompare"),
        VCS_EnablePositionWindow("VCS_EnablePositionWindow"),
        VCS_EnableVelocityWindow("VCS_EnableVelocityWindow"),
        VCS_FindHome("VCS_FindHome"),
        VCS_GetAllDigitalInputs("VCS_GetAllDigitalInputs"),
        VCS_GetAllDigitalOutputs("VCS_GetAllDigitalOutputs"),
        VCS_GetAnalogInput("VCS_GetAnalogInput"),
        VCS_GetAnalogInputState("VCS_GetAnalogInputState"),
        VCS_GetAnalogInputVoltage("VCS_GetAnalogInputVoltage"),
        VCS_GetBaudrateSelection("VCS_GetBaudrateSelection"),
        VCS_GetControllerGain("VCS_GetControllerGain"),
        VCS_GetCurrentIs("VCS_GetCurrentIs"),
        VCS_GetCurrentIsAveraged("VCS_GetCurrentIsAveraged"),
        VCS_GetCurrentMust("VCS_GetCurrentMust"),
        VCS_GetCurrentRegulatorGain("VCS_GetCurrentRegulatorGain"),
        VCS_GetDcMotorParameter("VCS_GetDcMotorParameter"),
        VCS_GetDeviceErrorCode("VCS_GetDeviceErrorCode"),
        VCS_GetDeviceName("VCS_GetDeviceName"),
        VCS_GetDeviceNameSelection("VCS_GetDeviceNameSelection"),
        VCS_GetDisableState("VCS_GetDisableState"),
        VCS_GetDriverInfo("VCS_GetDriverInfo"),
        VCS_GetEcMotorParameter("VCS_GetEcMotorParameter"),
        VCS_GetEnableState("VCS_GetEnableState"),
        VCS_GetEncoderParameter("VCS_GetEncoderParameter"),
        VCS_GetErrorInfo("VCS_GetErrorInfo"),
        VCS_GetFaultState("VCS_GetFaultState"),
        VCS_GetFreeIpmBufferSize("VCS_GetFreeIpmBufferSize"),
        VCS_GetGatewaySettings("VCS_GetGatewaySettings"),
        VCS_GetHallSensorParameter("VCS_GetHallSensorParameter"),
        VCS_GetHomingParameter("VCS_GetHomingParameter"),
        VCS_GetHomingState("VCS_GetHomingState"),
        VCS_GetIncEncoderParameter("VCS_GetIncEncoderParameter"),
        VCS_GetInterfaceName("VCS_GetInterfaceName"),
        VCS_GetInterfaceNameSelection("VCS_GetInterfaceNameSelection"),
        VCS_GetIpmBufferParameter("VCS_GetIpmBufferParameter"),
        VCS_GetIpmStatus("VCS_GetIpmStatus"),
        VCS_GetKeyHandle("VCS_GetKeyHandle"),
        VCS_GetMasterEncoderParameter("VCS_GetMasterEncoderParameter"),
        VCS_GetMaxAcceleration("VCS_GetMaxAcceleration"),
        VCS_GetMaxFollowingError("VCS_GetMaxFollowingError"),
        VCS_GetMaxProfileVelocity("VCS_GetMaxProfileVelocity"),
        VCS_GetMotorParameter("VCS_GetMotorParameter"),
        VCS_GetMotorType("VCS_GetMotorType"),
        VCS_GetMovementState("VCS_GetMovementState"),
        VCS_GetNbOfDeviceError("VCS_GetNbOfDeviceError"),
        VCS_GetObject("VCS_GetObject"),
        VCS_GetOperationMode("VCS_GetOperationMode"),
        VCS_GetPortName("VCS_GetPortName"),
        VCS_GetPortNameSelection("VCS_GetPortNameSelection"),
        VCS_GetPositionCompareParameter("VCS_GetPositionCompareParameter"),
        VCS_GetPositionIs("VCS_GetPositionIs"),
        VCS_GetPositionMarkerParameter("VCS_GetPositionMarkerParameter"),
        VCS_GetPositionMust("VCS_GetPositionMust"),
        VCS_GetPositionProfile("VCS_GetPositionProfile"),
        VCS_GetPositionRegulatorFeedForward("VCS_GetPositionRegulatorFeedForward"),
        VCS_GetPositionRegulatorGain("VCS_GetPositionRegulatorGain"),
        VCS_GetProtocolStackName("VCS_GetProtocolStackName"),
        VCS_GetProtocolStackNameSelection("VCS_GetProtocolStackNameSelection"),
        VCS_GetProtocolStackSettings("VCS_GetProtocolStackSettings"),
        VCS_GetQuickStopState("VCS_GetQuickStopState"),
        VCS_GetSensorType("VCS_GetSensorType"),
        VCS_GetSsiAbsEncoderParameter("VCS_GetSsiAbsEncoderParameter"),
        VCS_GetSsiAbsEncoderParameterEx("VCS_GetSsiAbsEncoderParameterEx"),
        VCS_GetState("VCS_GetState"),
        VCS_GetStepDirectionParameter("VCS_GetStepDirectionParameter"),
        VCS_GetTargetPosition("VCS_GetTargetPosition"),
        VCS_GetTargetVelocity("VCS_GetTargetVelocity"),
        VCS_GetVelocityIs("VCS_GetVelocityIs"),
        VCS_GetVelocityIsAveraged("VCS_GetVelocityIsAveraged"),
        VCS_GetVelocityMust("VCS_GetVelocityMust"),
        VCS_GetVelocityProfile("VCS_GetVelocityProfile"),
        VCS_GetVelocityRegulatorFeedForward("VCS_GetVelocityRegulatorFeedForward"),
        VCS_GetVelocityRegulatorGain("VCS_GetVelocityRegulatorGain"),
        VCS_GetVelocityUnits("VCS_GetVelocityUnits"),
        VCS_GetVersion("VCS_GetVersion"),
        VCS_HaltPositionMovement("VCS_HaltPositionMovement"),
        VCS_HaltVelocityMovement("VCS_HaltVelocityMovement"),
        VCS_MoveToPosition("VCS_MoveToPosition"),
        VCS_MoveWithVelocity("VCS_MoveWithVelocity"),
        VCS_OpenDevice("VCS_OpenDevice"),
        VCS_OpenSubDevice("VCS_OpenSubDevice"),
        VCS_ReadCANFrame("VCS_ReadCANFrame"),
        VCS_ReadPositionMarkerCapturedPosition("VCS_ReadPositionMarkerCapturedPosition"),
        VCS_ReadPositionMarkerCounter("VCS_ReadPositionMarkerCounter"),
        VCS_RequestCANFrame("VCS_RequestCANFrame"),
        VCS_ResetDevice("VCS_ResetDevice"),
        VCS_ResetPortNameSelection("VCS_ResetPortNameSelection"),
        VCS_ResetPositionMarkerCounter("VCS_ResetPositionMarkerCounter"),
        VCS_Restore("VCS_Restore"),
        VCS_SendCANFrame("VCS_SendCANFrame"),
        VCS_SendNMTService("VCS_SendNMTService"),
        VCS_SetAllDigitalOutputs("VCS_SetAllDigitalOutputs"),
        VCS_SetAnalogOutput("VCS_SetAnalogOutput"),
        VCS_SetAnalogOutputState("VCS_SetAnalogOutputState"),
        VCS_SetAnalogOutputVoltage("VCS_SetAnalogOutputVoltage"),
        VCS_SetControllerGain("VCS_SetControllerGain"),
        VCS_SetCurrentMust("VCS_SetCurrentMust"),
        VCS_SetCurrentRegulatorGain("VCS_SetCurrentRegulatorGain"),
        VCS_SetDcMotorParameter("VCS_SetDcMotorParameter"),
        VCS_SetDisableState("VCS_SetDisableState"),
        VCS_SetEcMotorParameter("VCS_SetEcMotorParameter"),
        VCS_SetEnableState("VCS_SetEnableState"),
        VCS_SetEncoderParameter("VCS_SetEncoderParameter"),
        VCS_SetGatewaySettings("VCS_SetGatewaySettings"),
        VCS_SetHallSensorParameter("VCS_SetHallSensorParameter"),
        VCS_SetHomingParameter("VCS_SetHomingParameter"),
        VCS_SetIncEncoderParameter("VCS_SetIncEncoderParameter"),
        VCS_SetIpmBufferParameter("VCS_SetIpmBufferParameter"),
        VCS_SetMasterEncoderParameter("VCS_SetMasterEncoderParameter"),
        VCS_SetMaxAcceleration("VCS_SetMaxAcceleration"),
        VCS_SetMaxFollowingError("VCS_SetMaxFollowingError"),
        VCS_SetMaxProfileVelocity("VCS_SetMaxProfileVelocity"),
        VCS_SetMotorParameter("VCS_SetMotorParameter"),
        VCS_SetMotorType("VCS_SetMotorType"),
        VCS_SetObject("VCS_SetObject"),
        VCS_SetOperationMode("VCS_SetOperationMode"),
        VCS_SetPositionCompareParameter("VCS_SetPositionCompareParameter"),
        VCS_SetPositionCompareReferencePosition("VCS_SetPositionCompareReferencePosition"),
        VCS_SetPositionMarkerParameter("VCS_SetPositionMarkerParameter"),
        VCS_SetPositionMust("VCS_SetPositionMust"),
        VCS_SetPositionProfile("VCS_SetPositionProfile"),
        VCS_SetPositionRegulatorFeedForward("VCS_SetPositionRegulatorFeedForward"),
        VCS_SetPositionRegulatorGain("VCS_SetPositionRegulatorGain"),
        VCS_SetProtocolStackSettings("VCS_SetProtocolStackSettings"),
        VCS_SetQuickStopState("VCS_SetQuickStopState"),
        VCS_SetSensorType("VCS_SetSensorType"),
        VCS_SetSsiAbsEncoderParameter("VCS_SetSsiAbsEncoderParameter"),
        VCS_SetSsiAbsEncoderParameterEx("VCS_SetSsiAbsEncoderParameterEx"),
        VCS_SetState("VCS_SetState"),
        VCS_SetStepDirectionParameter("VCS_SetStepDirectionParameter"),
        VCS_SetVelocityMust("VCS_SetVelocityMust"),
        VCS_SetVelocityProfile("VCS_SetVelocityProfile"),
        VCS_SetVelocityRegulatorFeedForward("VCS_SetVelocityRegulatorFeedForward"),
        VCS_SetVelocityRegulatorGain("VCS_SetVelocityRegulatorGain"),
        VCS_SetVelocityUnits("VCS_SetVelocityUnits"),
        VCS_StartIpmTrajectory("VCS_StartIpmTrajectory"),
        VCS_StopHoming("VCS_StopHoming"),
        VCS_StopIpmTrajectory("VCS_StopIpmTrajectory"),
        VCS_Store("VCS_Store"),
        VCS_WaitForHomingAttained("VCS_WaitForHomingAttained"),
        VCS_WaitForTargetReache("VCS_WaitForTargetReache");

        private String strCommand;

        Command(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }

    public enum ControllerGain {
        EC_PI_VELOCITY_CONTROLLER("EC_PI_VELOCITY_CONTROLLER"),
        EC_PI_VELOCITY_CONTROLLER_WITH_OBSERVER("EC_PI_VELOCITY_CONTROLLER_WITH_OBSERVER"),
        EC_PI_CURRENT_CONTROLLER("EC_PI_CURRENT_CONTROLLER"),
        EC_PID_POSITION_CONTROLLER("EC_PID_POSITION_CONTROLLER"),
        EC_DUAL_LOOP_POSITION_CONTROLLER("EC_DUAL_LOOP_POSITION_CONTROLLER"),

        EG_PICC_P_GAIN("EG_PICC_P_GAIN"),
        EG_PICC_I_GAIN("EG_PICC_I_GAIN"),
        EG_PIVC_P_GAIN("EG_PIVC_P_GAIN"),
        EG_PIVC_I_GAIN("EG_PIVC_I_GAIN"),
        EG_PIVC_FEED_FORWARD_VELOCITY_GAIN("EG_PIVC_FEED_FORWARD_VELOCITY_GAIN"),
        EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN("EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN"),

        EG_PIVCWO_P_GAIN("EG_PIVCWO_P_GAIN"),
        EG_PIVCWO_I_GAIN("EG_PIVCWO_I_GAIN"),
        EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN("EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN"),
        EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN("EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN"),
        EG_PIVCWO_OBSERVER_THETA_GAIN("EG_PIVCWO_OBSERVER_THETA_GAIN"),
        EG_PIVCWO_OBSERVER_OMEGA_GAIN("EG_PIVCWO_OBSERVER_OMEGA_GAIN"),
        EG_PIVCWO_OBSERVER_TAU_GAIN("EG_PIVCWO_OBSERVER_TAU_GAIN"),

        EG_PIDPC_P_GAIN("EG_PIDPC_P_GAIN"),
        EG_PIDPC_I_GAIN("EG_PIDPC_I_GAIN"),
        EG_PIDPC_D_GAIN("EG_PIDPC_D_GAIN"),
        EG_PIDPC_FEED_FORWARD_VELOCITY_GAIN("EG_PIDPC_FEED_FORWARD_VELOCITY_GAIN"),
        EG_PIDPC_FEED_FORWARD_ACCELERATION_GAIN("EG_PIDPC_FEED_FORWARD_ACCELERATION_GAIN"),
        ;

        private String strCommand;

        ControllerGain(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }

    public enum MotorType {
        MT_DC_MOTOR("MT_DC_MOTOR"),
        MT_EC_SINUS_COMMUTATED_MOTOR("MT_EC_SINUS_COMMUTATED_MOTOR"),
        MT_EC_BLOCK_COMMUTATED_MOTOR("MT_EC_BLOCK_COMMUTATED_MOTOR"),
        ;

        private String strCommand;

        MotorType(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum SensorType {
        ST_UNKNOWN("ST_UNKNOWN"),
        ST_INC_ENCODER_3CHANNEL("ST_INC_ENCODER_3CHANNEL"),
        ST_INC_ENCODER_2CHANNEL("ST_INC_ENCODER_2CHANNEL"),
        ST_HALL_SENSORS("ST_HALL_SENSORS"),
        ST_SSI_ABS_ENCODER_BINARY("ST_SSI_ABS_ENCODER_BINARY"),
        ST_SSI_ABS_ENCODER_GREY("ST_SSI_ABS_ENCODER_GREY"),
        ST_INC_ENCODER2_3CHANNEL("ST_INC_ENCODER2_3CHANNEL"),
        ST_INC_ENCODER2_2CHANNEL("ST_INC_ENCODER2_2CHANNEL"),
        ST_ANALOG_INC_ENCODER_3CHANNEL("ST_ANALOG_INC_ENCODER_3CHANNEL"),
        ST_ANALOG_INC_ENCODER_2CHANNEL("ST_ANALOG_INC_ENCODER_2CHANNEL"),
        ;

        private String strCommand;

        SensorType(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum DigitalInputConfig {
        DIC_NO_FUNCTIONALITY("DIC_NO_FUNCTIONALITY"),
        DIC_GENERAL_PURPOSE_A("DIC_GENERAL_PURPOSE_A"),
        DIC_GENERAL_PURPOSE_B("DIC_GENERAL_PURPOSE_B"),
        DIC_GENERAL_PURPOSE_C("DIC_GENERAL_PURPOSE_C"),
        DIC_GENERAL_PURPOSE_D("DIC_GENERAL_PURPOSE_D"),
        DIC_GENERAL_PURPOSE_E("DIC_GENERAL_PURPOSE_E"),
        DIC_GENERAL_PURPOSE_F("DIC_GENERAL_PURPOSE_F"),
        DIC_GENERAL_PURPOSE_G("DIC_GENERAL_PURPOSE_G"),
        DIC_GENERAL_PURPOSE_H("DIC_GENERAL_PURPOSE_H"),
        DIC_GENERAL_PURPOSE_I("DIC_GENERAL_PURPOSE_I"),
        DIC_GENERAL_PURPOSE_J("DIC_GENERAL_PURPOSE_J"),
        DIC_QUICK_STOP("DIC_QUICK_STOP"),
        DIC_DRIVE_ENABLE("DIC_DRIVE_ENABLE"),
        DIC_POSITION_MARKER("DIC_POSITION_MARKER"),
        DIC_HOME_SWITCH("DIC_HOME_SWITCH"),
        DIC_POSITIVE_LIMIT_SWITCH("DIC_POSITIVE_LIMIT_SWITCH"),
        DIC_NEGATIVE_LIMIT_SWITCH("DIC_NEGATIVE_LIMIT_SWITCH"),
        ;

        private String strCommand;

        DigitalInputConfig(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum Mask {
        True("True"),
        False("False"),
        ;

        private String strCommand;

        Mask(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum Polarity {
        ActiveLow("ActiveLow"),
        ActiveHigh("ActiveHigh"),
        ;

        private String strCommand;

        Polarity(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum OperationMode {
        OMD_PROFILE_POSITION_MODE("OMD_PROFILE_POSITION_MODE"),
        OMD_PROFILE_VELOCITY_MODE("OMD_PROFILE_VELOCITY_MODE"),
        OMD_HOMING_MODE("OMD_HOMING_MODE"),
        OMD_INTERPOLATED_POSITION_MODE("OMD_INTERPOLATED_POSITION_MODE"),
        OMD_POSITION_MODE("OMD_POSITION_MODE"),
        OMD_VELOCITY_MODE("OMD_VELOCITY_MODE"),
        OMD_CURRENT_MODE("OMD_CURRENT_MODE"),
        OMD_MASTER_ENCODER_MODE("OMD_MASTER_ENCODER_MODE"),
        OMD_STEP_DIRECTION_MODE("OMD_STEP_DIRECTION_MODE"),
        ;

        private String strCommand;

        OperationMode(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum HomingMethod {
        HM_ACTUAL_POSITION("HM_ACTUAL_POSITION"),
        HM_INDEX_POSITIVE_SPEED("HM_INDEX_POSITIVE_SPEED"),
        HM_INDEX_NEGATIVE_SPEED("HM_INDEX_NEGATIVE_SPEED"),
        HM_HOME_SWITCH_NEGATIVE_SPEED("HM_HOME_SWITCH_NEGATIVE_SPEED"),
        HM_HOME_SWITCH_POSITIVE_SPEED("HM_HOME_SWITCH_POSITIVE_SPEED"),
        HM_POSITIVE_LIMIT_SWITCH("HM_POSITIVE_LIMIT_SWITCH"),
        HM_NEGATIVE_LIMIT_SWITCH("HM_NEGATIVE_LIMIT_SWITCH"),
        HM_HOME_SWITCH_NEGATIVE_SPEED_AND_INDEX("HM_HOME_SWITCH_NEGATIVE_SPEED_AND_INDEX"),
        HM_HOME_SWITCH_POSITIVE_SPEED_AND_INDEX("HM_HOME_SWITCH_POSITIVE_SPEED_AND_INDEX"),
        HM_POSITIVE_LIMIT_SWITCH_AND_INDEX("HM_POSITIVE_LIMIT_SWITCH_AND_INDEX"),
        HM_NEGATIVE_LIMIT_SWITCH_AND_INDEX("HM_NEGATIVE_LIMIT_SWITCH_AND_INDEX"),
        HM_CURRENT_THRESHOLD_NEGATIVE_SPEED_AND_INDEX("HM_CURRENT_THRESHOLD_NEGATIVE_SPEED_AND_INDEX"),
        HM_CURRENT_THRESHOLD_POSITIVE_SPEED_AND_INDEX("HM_CURRENT_THRESHOLD_POSITIVE_SPEED_AND_INDEX"),
        HM_CURRENT_THRESHOLD_POSITIVE_SPEED("HM_CURRENT_THRESHOLD_POSITIVE_SPEED"),
        HM_CURRENT_THRESHOLD_NEGATIVE_SPEED("HM_CURRENT_THRESHOLD_NEGATIVE_SPEED"),
        ;

        private String strCommand;

        HomingMethod(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum HomingStatus {
        True("True"),
        False("False"),
        ;

        private String strCommand;

        HomingStatus(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum State {
        ST_DISABLED("ST_DISABLED"),
        ST_ENABLED("ST_ENABLED"),
        ST_QUICKSTOP("ST_QUICKSTOP"),
        ST_FAULT("ST_FAULT"),
        ;

        private String strCommand;

        State(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
    public enum temp {
        temp("temp"),
        ;

        private String strCommand;

        temp(String str) {
            this.strCommand = str;
        }

        public String string() {
            return this.strCommand;
        }
    }
}
